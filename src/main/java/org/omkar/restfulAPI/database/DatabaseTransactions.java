package org.omkar.restfulAPI.database;
import java.util.HashMap;
import java.util.Map;
import org.omkar.restfulAPI.model.*;

public class DatabaseTransactions {
	private static Map<Long,Message> messages=new HashMap<Long,Message>();
	private static Map<String,Profile> profiles=new HashMap<String,Profile>();
	
	static{	
		// Message values kept in map to perform CRUD instead of database  
			messages.put(1L, new Message(1L, "David Soloman", "Welcome NewYorkers!!"));
			messages.put(2L, new Message(2L, "Mark Phillips", "Manhattan is an awesome place!!"));
			
	    // Profile values kept in map to perform CRUD instead of database  
			profiles.put("zlatan", new Profile(1L,"zlatan763434","zlatan","Ibhramovic"));
			profiles.put("johny", new Profile(2L,"johny9786","johny","Cage"));
	}
	
	public static Map<Long,Message> getMessages(){
		return messages;
	}
	
	public static Map<String, Profile> getProfiles(){
		return profiles;
	}
}
