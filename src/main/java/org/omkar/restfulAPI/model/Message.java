package org.omkar.restfulAPI.model;

import java.util.Date;

public class Message {

	private long messageId;
	private String author=null;
	private String message=null;
	private Date creationDate=null;
	
	
	
	public Message() {
	}
	
	public Message(long messageId, String author, String message) {

		this.messageId = messageId;
		this.author = author;
		this.message = message;
		this.creationDate=new Date();
	}
	public long getMessageId() {
		return messageId;
	}
	public void setMessageId(long messageId) {
		this.messageId = messageId;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	
}
