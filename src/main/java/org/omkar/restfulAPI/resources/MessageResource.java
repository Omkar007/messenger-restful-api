package org.omkar.restfulAPI.resources;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.omkar.restfulAPI.model.Message;
import org.omkar.restfulAPI.services.MessageService;

@Path("/messages")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class MessageResource {

	MessageService messageService=new MessageService();
	
	@GET
	public List<Message> getMessages(@QueryParam("year") int year,@QueryParam("startIndex") int startIndex,@QueryParam("startIndex") int size){
		
		if(year>0){
			return messageService.getMessagesforYear(year);
		}
		else if(startIndex>0 && size>0){
			return messageService.getPaginationMessage(startIndex, size);
		}
		else{
			return messageService.getAllMessages();	
		}
		
	}
	
	@GET
	@Path("/{messageId}")
	public Message getMessage(@PathParam("messageId") long messageId){
		return messageService.getMessage(messageId);
	}
	
	@POST
	public Message addMessage(Message msg){
		return messageService.addMessage(msg);
	}
	
	@PUT
	@Path("/{messageId}")
	public Message updateMessage(@PathParam("messageId") long messageId,Message msg){
		msg.setMessageId(messageId);
		return messageService.updateMessage(msg);
	}
	
	@DELETE
	@Path("/{messageId}")
	public void deleteMessage(@PathParam("messageId") long messageId){
		messageService.deleteMessage(messageId);
	}
}
