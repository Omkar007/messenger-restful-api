package org.omkar.restfulAPI.services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import org.omkar.restfulAPI.database.DatabaseTransactions;
import org.omkar.restfulAPI.model.Message;


public class MessageService {
	private static Map<Long,Message> messages=DatabaseTransactions.getMessages();


	public List<Message> getAllMessages(){
		return new ArrayList<Message>(messages.values());
	}
	
	public List<Message> getMessagesforYear(int year){

		List<Message> messagesYear=new ArrayList<>();
		Calendar cal=Calendar.getInstance();
		
		for(Message msg: messages.values()){
			cal.setTime(msg.getCreationDate());
			if((cal.get(Calendar.YEAR)==year)){
				messagesYear.add(msg);
			}
		}
		return messagesYear;
	}
	
	public List<Message> getPaginationMessage(int startIndex,int size){
		List<Message> paginatedMsgs=new ArrayList<>(messages.values());
		if((startIndex+size)> paginatedMsgs.size()){
			return null;
		}
		else{
			return paginatedMsgs.subList(startIndex, startIndex+size); 
		}
	}

	public Message getMessage(long messageId){
		return messages.get(messageId);
	}

	public Message addMessage(Message msg){
		msg.setMessageId(messages.size()+1);
		messages.put(msg.getMessageId(), msg);
		return msg;
	}

	public Message updateMessage(Message msg){
		if(msg.getMessageId()<=0){
			return null;
		}
		else{
			messages.put(msg.getMessageId(), msg);
			return msg;
		}
	}

	public Message deleteMessage(long messageID){
		return messages.remove(messageID);

	}
}
