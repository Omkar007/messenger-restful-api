package org.omkar.restfulAPI.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.omkar.restfulAPI.database.DatabaseTransactions;
import org.omkar.restfulAPI.model.Profile;

public class ProfileService {

	private static Map<String, Profile> profiles=DatabaseTransactions.getProfiles();

	public List<Profile> getAllProfiles(){
		return new ArrayList<Profile>(profiles.values());
	}

	public Profile getProfile(String profileName){
		return profiles.get(profileName);
	}

	public Profile addProfile(Profile profile){
		profile.setProfileId(profiles.size()+1);
		profiles.put(profile.getProfileName(), profile);
		return profile;
	}

	public Profile updateProfile(Profile profile){
		if(!profile.getProfileName().isEmpty()){		
			profiles.put(profile.getProfileName(), profile);
			return profile;
		}
		else{
			return null;
		}
	}

	public Profile deleteProfile(String profileName){
		return profiles.remove(profileName);

	}
}
